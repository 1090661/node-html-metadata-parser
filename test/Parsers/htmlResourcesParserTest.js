process.env.NODE_ENV = 'test';
const chai = require('chai');
const leche = require('leche');
const HtmlResourcesParser = require('../../src/Parsers/htmlResourcesParser');
const withData = leche.withData;

describe('Check resources loaded on page', () => {

    let htmlResourcesParser = new HtmlResourcesParser();

    withData({
        test_small_html: [
            '<html><head><link rel="stylesheet" href="https://test.com/test.css"></head><body><span class="hello">Hello</span></body></html>',
            [
                {
                    url: 'https://test.com/test.css',
                    tag: 'link',
                    extension: 'css'
                }
            ]
        ],
        test_small_html_2: [
            '<html><head><meta></meta><meta></meta><style></style></head><body><p><span><span><span><script src="http://thehost.io/test.js"></script>' +
            '<span class="deep"></span></span></span></span></p></body></html>',
            [
                {
                    url: 'http://thehost.io/test.js',
                    tag: 'script',
                    extension: 'js'
                }
            ]
        ],
        test_loading_script_inside_script: [
            '<html><head></head><body><script>$.ajax({ url: "http://test.js", success: function(result){}});</script></body></html>',
            [
                {
                    url: 'http://test.js',
                    extension: 'js'
                }
            ]
        ]

    }, (input, expectedResult) => {
        it('Evaluate if every downloadable resource from the page can be identified', () => {
            chai.assert.deepEqual(htmlResourcesParser.parseHtml(input), expectedResult);
        });
    });

});