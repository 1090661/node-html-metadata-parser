process.env.NODE_ENV = 'test';
const chai = require('chai');
const leche = require('leche');
const HtmlTreeMapParser = require('../../src/Parsers/htmlTreeMapParser');
const withData = leche.withData;

describe('Child nodes count and tree depth test', () => {

    let htmlTreeMapParser = new HtmlTreeMapParser();

    withData({
        test_small_html: [
            '<html><head></head><body><a href="test.com"><span class="hello">Hello</span></a></body></html>',
            {
                map: {
                    html: {
                        head: 1,
                        body: 1
                    },
                    head: {},
                    body: {
                        a: 1
                    },
                    a: {
                        span: 1
                    },
                    span: {}
                },
                depth: 3
            }
        ],
        test_small_html_2: [
            '<html><head><meta></meta><meta></meta><style></style></head><body><p><span><span><span><span class="deep"></span></span></span></span></p></body></html>',
            {
                map: {
                    html: {
                        head: 1,
                        body: 1
                    },
                    head: {
                        meta: 2,
                        style: 1
                    },
                    meta: {},
                    style: {},
                    body: {
                        p: 1
                    },
                    p: {
                        span: 1
                    },
                    span: {
                        span: 3
                    }
                },
                depth: 4
            }
        ]

    }, (input, expectedResult) => {
        it('Evaluate if depth and childs by type are being counted correctly', () => {

            chai.assert.deepEqual(htmlTreeMapParser.parseHtml(input), expectedResult);

        });
    });


    it('Test get depth of tree', () => {
        let tree = "<html><head></head><body><div><div><div><span>OLA</span></div></div></div></body></html>"
        const $ = htmlTreeMapParser.load(tree);
        chai.assert.equal(htmlTreeMapParser.depthOfTree($('html')), 4);
    });


});