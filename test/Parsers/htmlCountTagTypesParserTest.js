process.env.NODE_ENV = 'test';
const chai = require('chai');
const leche = require('leche');
const HtmlCountTagTypesParser = require('../../src/Parsers/htmlCountTagTypesParser');
const withData = leche.withData;

describe('Count different tag types test', () => {

    let htmlCountTagTypes = new HtmlCountTagTypesParser();

    withData({
        test_small_html: [
            '<html><head></head><body><a href="test.com"><span class="hello">Hello</span></a></body></html>',
            {
                html: 1,
                head: 1,
                body: 1,
                a: 1,
                span: 1
            }
        ],
        test_small_html_2: [
            '<html><head><meta></meta><meta></meta><style></style></head><body><p><span><span><span><span class="deep"></span></span></span></span></p></body></html>',
            {
                html: 1,
                head: 1,
                meta: 2,
                style: 1,
                body: 1,
                p: 1,
                span: 4
            }
        ]

    }, (input, expectedResult) => {
        it('Evaluate if the html count tags by type is working', () => {
            chai.assert.deepEqual(htmlCountTagTypes.parseHtml(input), expectedResult);
        });
    });

});