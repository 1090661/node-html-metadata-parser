process.env.NODE_ENV = 'test';
const chai = require('chai');
const leche = require('leche');
const HtmlAttributesByTagTypeParser = require('../../src/Parsers/htmlAttributesByTagTypeParser');
const withData = leche.withData;

describe('Count attributes by tag type', () => {

    let htmlAttributesByTagTypeParser = new HtmlAttributesByTagTypeParser();

    withData({
        test_small_html: [
            '<html><head></head><body background="red"><a href="test.com"><span class="hello">Hello</span></a></body></html>',
            {
                body: {
                    background: 1
                },
                a: {
                    href: 1
                },
                span: {
                    class: 1
                }
            }
        ]
    }, (input, expectedResult) => {
        it('Evaluate if the html attribute type by tag type count is working', () => {
            chai.assert.deepEqual(htmlAttributesByTagTypeParser.parseHtml(input), expectedResult);
        });
    });

});