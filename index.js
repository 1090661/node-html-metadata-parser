const MetadataExtractorCli = require('./src/MetadataExtractorCli');
const validUrl = require('valid-url');
const logger = require('./src/Utils/logger');

let url = process.argv[2];
let method = process.argv[3] || 'GET';

url = !url || /^https?\:\/\//.test(url) ? url : 'http://' + url;

if (!validUrl.isWebUri(url)) {
    logger.warn("Please input a valid URL as first argument");
    logger.info("Example: node index.js <url> <method (optional) - default `GET`>")
    return -1;
}

let metadataExtractorCli = new MetadataExtractorCli();
metadataExtractorCli.run(url, method);

