## Node.js HTML Metadata Crawler

### Quick Start

To use this application you need to have node runtime installed and npm package manager.

Then run the following steps:

``` npm install ```

``` npm start http://google.com ``` or 

``` node index.js http://google.com ``` 

### Run tests

If you wish to run the test suite for this project you can use

``` npm test ```

### Dependencies

This project uses some dependencies to ease and speed up development:

- [cheerio](https://github.com/cheeriojs/cheerio) - DOM parser that provides jQuery like API
- [request](https://github.com/request/request) - Super easy http client
- [valid-url](https://www.npmjs.com/package/valid-url) - Simple url validation
- [winston](https://github.com/winstonjs/winston) - Logger library
- [chai](http://chaijs.com/) - Assertion framework for unit testing
- [mocha](https://mochajs.org/) - The test runner
- [leche](https://github.com/box/leche) - Utility to add some extensions to Mocha

### Technical details

This application works by downloading the HTML from a given URL and running the html string through an array of parsers.

The class ```MetadataParser``` will run the html string through each of the available parsers:

```js
	this.availableParsers = {
		htmlTreeInfo: new HtmlTreeMapParser(),
		resourcesMetadata: new HtmlResourcesParser(),
		tagCountByTypes: new HtmlCountTagTypesParser(),
   		attributesInfo: new HtmlAttributesByTagTypeParser()
	} 
```
Each parser will return different metadata. 

In the end metadata is assigned to an object and logged to the console.