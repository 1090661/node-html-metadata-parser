const cheerio = require('cheerio');
const util = require('util');
const logger = require('../Utils/logger');

class HtmlTreeMapParser {

    constructor() {

    }

    /**
     * Parses an html string to obtain metadata about the DOM
     * @param {string} htmlString 
     * 
     * @returns {*}
     */
    parseHtml(htmlString) {
        logger.info("Building an object with html child counts and tree depth");
        const $ = this.load(htmlString);

        let treeMap = {};

        $('*').each((i, elem) => {
            const tag = elem.tagName;
            treeMap[tag] = treeMap[tag] || {};
            $(elem).children().each((i, childElement) => {
                const childTag = childElement.tagName;
                treeMap[tag][childTag] = treeMap[tag][childTag] + 1 || 1;
            });
        });
        return {
            map: treeMap,
            depth: this.depthOfTree($('html'))
        }
    }

    /**
     * Obtain depth of dom element
     * @param {CheerioElement} element 
     */
    depthOfTree(rootElement) {
        let childs = rootElement.children();
        if (childs.length === 0) {
            return 1;
        }
        return 1 + this.depthOfTree(childs.children());
    }

    load(htmlString) {
        return cheerio.load(htmlString);
    }

}

module.exports = HtmlTreeMapParser;