const cheerio = require('cheerio');
const logger = require('../Utils/logger');

class HtmlCountTagTypesParser {

    constructor() {

    }

    /**
     * Parses html string to obtain the count for each tag type found
     * 
     * @param {*} htmlString 
     * @returns {*}
     */
    parseHtml(htmlString) {
        logger.info("Counting html tag types");
        const $ = cheerio.load(htmlString);
        let htmlTypesCount = {};

        $('*').each((i, elem) => {
            const tag = elem.tagName;
            htmlTypesCount[tag] = htmlTypesCount[tag] + 1 || 1;
        });

        return htmlTypesCount;
    }

}

module.exports = HtmlCountTagTypesParser;