const cheerio = require('cheerio');
const logger = require('../Utils/logger');

class HtmlAttributesByTagTypeParser {

    constructor() {

    }

    /**
     * Parses html string to count attributes by each tag type with attributes found
     * 
     * @param {*} htmlString 
     * @returns {*}
     */
    parseHtml(htmlString) {
        logger.info("Counting attributes by tag type");
        const $ = cheerio.load(htmlString);
        let attributesCountByTag = {};

        $('*').each((i, elem) => {
            const tag = elem.tagName;

            if (!Object.keys(elem.attribs).length) return;
            attributesCountByTag[tag] = attributesCountByTag[tag] || {};

            for (let attrKey in elem.attribs) {
                attributesCountByTag[tag][attrKey] = attributesCountByTag[tag][attrKey] + 1 || 1;
            }

        });

        return attributesCountByTag;
    }

}

module.exports = HtmlAttributesByTagTypeParser;