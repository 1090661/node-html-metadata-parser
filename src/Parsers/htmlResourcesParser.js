const request = require('request');
const cheerio = require('cheerio');
const util = require('util');
const logger = require('../Utils/logger');

class HtmlResourcesParser {

    constructor() {
        //This regex should be improved has many flaws like matching unexistent TLD's like qweqwe.qweqwe.qweqe
        // It will be used to search possible files being loaded inside scripts
        this.urlRegex = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/igm;
    }

    /**
     * Parses html string to obtain what kind of resources were loaded 
     * on the page and the hosts to which they were requested
     * 
     * @param {*} htmlString 
     * @returns {*}
     */
    parseHtml(htmlString) {
        logger.info("Parsing HTML to find resources that are downloaded");
        const $ = cheerio.load(htmlString);
        let resourcesLoadedOnHtml = [];

        $('script,[src],[href]').each((i, elem) => {
            const tag = elem.tagName;

            if (tag === 'a') return; //Just ignore links, they can harm if you click but cannot download stuff on their own i guess

            let url = elem.attribs['src'] ? elem.attribs['src'] : elem.attribs['href'];

            if (tag === 'script' && !url) { //Inline script, parse urls inside of the script

                /* The way this is implemented can lead to a lot of false positives for example
                links in docs would appear on the array */
                resourcesLoadedOnHtml = resourcesLoadedOnHtml.concat(this.addMatchesBasedOnRegex($(elem).html()));
                return;
            }

            let extension = url ? url.split(".").pop() : null;
            resourcesLoadedOnHtml.push({
                url: url,
                extension: extension,
                tag: tag
            });
        });

        /** 
         * This parsing can be even more improved by crawling sources inside other scripts to check for the ones
         * that run more scripts on page
        */

        logger.info("Ended parsing resources on HTML");
        return resourcesLoadedOnHtml;
    }

    /**
     * Search on a string for url matches
     * @param {string} scriptContent 
     */
    addMatchesBasedOnRegex(scriptContent) {
        let moreSuspects = [];
        let possibleMatches = scriptContent.match(this.urlRegex);

        if (!possibleMatches || !possibleMatches.length) return [];

        for (let match of possibleMatches) {
            moreSuspects.push({
                url: match,
                extension: match.split(".").pop()
            });
        }
        return moreSuspects;
    }

}

module.exports = HtmlResourcesParser;