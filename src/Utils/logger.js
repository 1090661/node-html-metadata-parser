const winston = require('winston');

class Logger {

    constructor() {
        this.disableConsole = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'test';
        this.logger = winston.createLogger({
            level: 'info',
            format: winston.format.json(),
            transports: [
                new winston.transports.File({ filename: 'logs.log', level: 'info' }),
            ]
        });
        
        if (!this.disableConsole) {
            this.logger.add(new winston.transports.Console({
                format: winston.format.simple()
            }));
        }
    }

    /**
     * Return configured instance of winston logger
     * 
     * @returns {*}
     */
    getLogger() {
        return this.logger;
    }
}

const logger = new Logger();
module.exports = logger.getLogger();