const HtmlTreeMapParser = require('./Parsers/htmlTreeMapParser');
const HtmlResourcesParser = require('./Parsers/htmlResourcesParser');
const HtmlCountTagTypesParser = require('./Parsers/htmlCountTagTypesParser');
const HtmlAttributesByTagTypeParser = require('./Parsers/htmlAttributesByTagTypeParser');
const request = require('request');
const logger = require('./Utils/logger');

class MetadataParser {

    constructor(url, method) {
        this.url = url;
        this.method = method;
        this.metadata = {};
        this.availableParsers = {
            htmlTreeInfo: new HtmlTreeMapParser(),
            resourcesMetadata: new HtmlResourcesParser(),
            tagCountByTypes: new HtmlCountTagTypesParser(),
            attributesInfo: new HtmlAttributesByTagTypeParser()
        }
    }

    /**
     * Collects metadata from the HTML fetched from an url
     * When data is ready the callback is executed
     * 
     * @param {function} onMetadataReadyCallback 
     */
    collectMetadata(onMetadataReadyCallback) {
        logger.info(`Using method ${this.method} to request ${this.url}`);
        request(
            {
                method: this.method,
                uri: this.url
            },
            (err, response, body) => {
                if (err) {
                    logger.error("Error requesting the given URL:", err);
                    return;
                }
                this._parse(body);
                onMetadataReadyCallback(this.metadata);
            }
        );
    }

    /**
     * Use the defined parsers to extract information and put it in metadata object
     * @param {string} htmlString 
     */
    _parse(htmlString) {
        for (let key in this.availableParsers) {
            this.metadata[key] = this.availableParsers[key].parseHtml(htmlString);
        }
    }

}

module.exports = MetadataParser;