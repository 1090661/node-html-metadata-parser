const url = require('url');
const MetadataParser = require('./metadataParser');
const logger = require('./Utils/logger');
const util = require('util');

class MetadataExtractorCli {

    constructor() {

    }

    /**
     * Entrypoint for the CLI application
     * 
     * @param {string} urlString 
     * @param {string} method
     */
    run(urlString, method) {

        method = method || 'GET';
        let metadataParser = new MetadataParser(urlString, method);
        metadataParser.collectMetadata((metadata) => {
            logger.info("Extracted the following metadata: ");
            logger.info(util.inspect(metadata, true, null));
        });
    }
}

module.exports = MetadataExtractorCli;